#! /bin/bash

# instructions
help() {
    echo "development environment creator"
    echo "$ ./run.sh --local /path/to/local/repo "
}

# get command line args 
POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -l|--local)
    LOCALPATH="$2"
    shift # past argument
    shift # past value
    ;;
    -h|--help)
    HELP="$1"
    shift # past argument
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters


if [ $HELP ] || [ -z "${LOCALPATH}" ]; then
    help
    exit 1
fi 

until docker ps > /dev/null
do
    echo "Waiting for docker server"
    sleep 1
done

echo "Starting Development container (PPAVP) ..."

NVIDIA_DRIVER_VERSION=$(nvidia-smi --query-gpu=driver_version --format=csv,noheader | head -n 1 | cut -d '.' -f 1)

mkdir -p ${LOCALPATH}/devel
mkdir -p ${LOCALPATH}/build


docker run -it --rm \
       -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix \
       -v /usr/lib/nvidia-${NVIDIA_DRIVER_VERSION}:/usr/lib/nvidia-${NVIDIA_DRIVER_VERSION} \
       -e LD_LIBRARY_PATH=/usr/lib/nvidia-${NVIDIA_DRIVER_VERSION} \
       --privileged \
       --runtime=nvidia \
       --device=/dev/input/js0 \
       -v ${LOCALPATH}:/home/developer/workspace/src/ppavp \
       -v ${LOCALPATH}/devel:/home/developer/workspace/devel \
       -v ${LOCALPATH}/build:/home/developer/workspace/build \
       -v $HOME:/hosthome \
       dev_ppavp:latest $@


