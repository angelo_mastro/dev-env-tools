#! /bin/bash

# instructions
help() {
    echo "development environment creator"
    echo "$ ./build.sh --image <docker_img_name> [--ide ALL (default) | QT | PYCHARM | CLION | NETBEANS] "
}


fillImage() {
    if [ $1 ]; then
        echo "Creating Dockerfile for image ${2} ... "
        echo "FROM $2" >> $1  
        echo "" >> $1
        echo "" >> $1
    fi
}

fillQtCreator() {
    echo "Adding QtCreator to Dockerfile ... "

    echo "# Qtcreator " >> $1

    # install QtCreator
    echo "RUN sudo apt-get update && sudo apt-get install -y qtcreator" >> $1
    
    

    # add specific configuration
    echo "RUN sudo mkdir ~/.config" >> $1
    echo "RUN sudo mkdir ~/.config/QtProject" >> $1
    echo "RUN sudo chown \$(whoami):\$(whoami) -R ~/.config" >> $1
    echo "RUN touch ~/.config/QtProject/QtCreator.ini" >> $1

    echo "RUN echo \"[TextEditor]\" >> ~/.config/QtProject/QtCreator.ini ; " \
    "echo \"ColorScheme=/usr/share/qtcreator/styles/inkpot.xml\" >> ~/.config/QtProject/QtCreator.ini ; " \
    "echo \"\" >> ~/.config/QtProject/QtCreator.ini ;  " \
    "echo \"[textMarginSettings]\" >> ~/.config/QtProject/QtCreator.ini ;  " \
    "echo \"MarginColumn=80\" >> ~/.config/QtProject/QtCreator.ini ; " \
    "echo \"ShowMargin=true\" >> ~/.config/QtProject/QtCreator.ini ;  " \
    "echo \"\" >> ~/.config/QtProject/QtCreator.ini ;  " \
    "echo \"[textTabPreferences]\" >> ~/.config/QtProject/QtCreator.ini ;  " \
    "echo \"AutoSpacesForTabs=false\" >> ~/.config/QtProject/QtCreator.ini ;  " \
    "echo \"IndentSize=4\" >> ~/.config/QtProject/QtCreator.ini ;  " \
    "echo \"PaddingMode=1\" >> ~/.config/QtProject/QtCreator.ini ;  " \
    "echo \"SpacesForTabs=true\" >> ~/.config/QtProject/QtCreator.ini ;  " \
    "echo \"TabSize=4\" >> ~/.config/QtProject/QtCreator.ini ;  " \
    "echo \"\" >> ~/.config/QtProject/QtCreator.ini ;  " \
    "echo \"[ProjectExplorer]\" >> ~/.config/QtProject/QtCreator.ini ;  " \
    "echo \"Settings\BuildBeforeDeploy=false\" >> ~/.config/QtProject/QtCreator.ini ;  " \
    "echo \"Settings\SaveBeforeBuild=false\" >> ~/.config/QtProject/QtCreator.ini ;  " \
    "echo \"Settings\DeployBeforeRun=false\" >> ~/.config/QtProject/QtCreator.ini ;  " \
    "echo \"\" >> ~/.config/QtProject/QtCreator.ini ;  " \
    "echo \"[textStorageSettings]\" >> ~/.config/QtProject/QtCreator.ini ;  " \
    "echo \"addFinalNewLine=true\" >> ~/.config/QtProject/QtCreator.ini ;  " \
    "echo \"cleanIndentation=true\" >> ~/.config/QtProject/QtCreator.ini ;  " \
    "echo \"cleanWhitespace=true\" >> ~/.config/QtProject/QtCreator.ini ;  " \
    "echo \"inEntireDocument=true\" >> ~/.config/QtProject/QtCreator.ini ;  " \
    "echo \"\" >> ~/.config/QtProject/QtCreator.ini" >> $1
    
    echo "" >> $1
    echo "" >> $1
}

fillPycharm() {
    echo "Adding Pycharm to Dockerfile ... "

    echo "# Pycharm" >> $1
    
    # dependencies
    echo "RUN sudo apt-get update" >> $1
    echo "RUN sudo apt-get install -y python-dev" >> $1
    echo "RUN sudo apt-get install -y python-pip" >> $1
    echo "RUN sudo pip install --upgrade pip" >> $1
    
    echo "RUN sudo apt-get update && sudo apt-get upgrade -y" >> $1
    echo "RUN sudo apt-get install -y software-properties-common python-software-properties" >> $1
    echo "RUN sudo add-apt-repository ppa:mystic-mirage/pycharm" >> $1
    echo "RUN sudo apt-get update" >> $1
    echo "RUN sudo apt-get install -y pycharm-community" >> $1
    echo "RUN sudo ln -s /usr/bin/pycharm-community /usr/bin/pycharm" >> $1
    
    echo "" >> $1
    echo "" >> $1
}

fillClion() {
    echo "Adding CLion to Dockerfile ... "
    
    echo "# CLion " >> $1

    # dependencies 
    echo "RUN sudo apt-get update " >> $1
    echo "RUN sudo apt-get install -y software-properties-common" >> $1
    echo "RUN sudo add-apt-repository ppa:webupd8team/java" >> $1
    echo "RUN sudo apt-get update" >> $1
    echo "RUN sudo echo debconf shared/accepted-oracle-license-v1-1 select true ; sudo debconf-set-selections" >> $1
    echo "RUN sudo echo debconf shared/accepted-oracle-license-v1-1 seen true ; sudo debconf-set-selections" >> $1
    echo "RUN sudo apt-get install -y java-common oracle-java8-installer" >> $1
    echo "RUN sudo apt-get update" >> $1

    # install Clion 
    echo "RUN sudo mkdir ~/Download" >> $1
    echo "RUN cd ~/Download" >> $1
    echo "RUN wget https://download.jetbrains.com/cpp/CLion-2016.1.3.tar.gz" >> $1
    echo 'RUN tar xzf CLion*.tar.gz -C ~/' >> $1

    # create a link to it
    echo "RUN sudo ln -s ~/clion*/bin/clion.sh /usr/bin/clion" >> $1
    
    echo "" >> $1
    echo "" >> $1
}


fillNetBeans() {
    echo "Adding Netbeans to Dockerfile ... "
    
    echo "# Netbeans" >> $1
    
    # doucle check (java dependencies)
    echo "RUN sudo apt-get update && sudo apt-get upgrade -y" >> $1
    echo "RUN sudo apt-get install -y default-jre" >> $1
    echo "RUN sudo apt-get install -y default-jdk" >> $1
    echo "RUN sudo apt-get update && sudo apt-get install --yes software-properties-common" >> $1

    echo "RUN sudo add-apt-repository ppa:webupd8team/java" >> $1
    echo "RUN sudo apt-get update" >> $1
    echo "RUN sudo echo debconf shared/accepted-oracle-license-v1-1 select true ; sudo debconf-set-selections" >> $1
    echo "RUN sudo echo debconf shared/accepted-oracle-license-v1-1 seen true ; sudo debconf-set-selections" >> $1
    echo "RUN sudo apt-get install -y oracle-java8-installer" >> $1

    echo "RUN sudo echo \"JAVA_HOME=/usr/lib/jvm/java-8-oracle\" >> ~/.profile" >> $1
    echo "RUN sudo echo \"JRE_HOME=/usr/lib/jvm/java-8-oracle/jre\" >> ~/.profile" >> $1
    echo "RUN sudo /bin/bash -c \"source ~/.profile\"" >> $1

    # install netbeans 8.1 (careful to the version!)
    echo "RUN sudo add-apt-repository \"deb http://archive.ubuntu.com/ubuntu $(lsb_release -sc) universe\"" >> $1
    echo "RUN sudo apt-get update" >> $1
    echo "RUN sudo apt-get install -y netbeans" >> $1
    
    echo "" >> $1
    echo "" >> $1
}


fillIDE() {
    if [ -z $2 ]; then
        fillQtCreator $1
        fillPycharm $1
        fillClion $1
        fillNetBeans $1
    elif [ $2 == "QT" ]; then
        fillQtCreator $1
    elif [ $2 == "PYCHARM" ]; then
        fillPycharm $1
    elif [ $2 == "CLION" ]; then
        fillClion $1
    elif [ $2 == "NETBEANS" ]; then
        fillNetBeans $1
    fi
}

fillCAD() {
    echo "Adding FreeCAD to Dockerfile ... "
    
    echo "# CAD" >> $1
    echo "RUN sudo apt-get install -y freecad " >> $1
    echo "RUN printf \"\n# CAD settings\nexport PYTHONPATH=\'\$PYTHONPATH:/usr/lib/freecad/lib:/usr/lib/freecad/lib:/usr/lib/freecad/Mod\'\n\" >> ~/.profile"
    echo "/bin/bash -c source ~/.profile"
    
    echo "" >> $1
    echo "" >> $1

}

fillEntryPoint() {
    # start in the terminal 
    echo "CMD \"/bin/bash\"" >> $1
}

# get command line args 
POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -i|--image)
    IMAGE="$2"
    shift # past argument
    shift # past value
    ;;
    -d|--ide)
    IDE="$2"
    shift # past argument
    shift # past value
    ;;
    -c|--cad)
    CAD="$1"
    shift # past argument
    ;;    
    -h|--help)
    HELP="$1"
    shift # past argument
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters


if [ $HELP ] || [ -z "${IMAGE}" ]; then
    help
    exit 1
fi 


rm Dockerfile
touch Dockerfile

fillImage Dockerfile ${IMAGE}

fillIDE Dockerfile ${IDE}

if [ $CAD ]; then
    fillCAD Dockerfile
fi

fillEntryPoint Dockerfile

# build the image 
docker build -t "dev_${IMAGE}" .



