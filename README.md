# Shared Development Environment Tools

This repo makes you build a Docker image as a layer standing on top of a previously built image with the working repo. 

### How it works

Assuming you have already build the imagage of your repo, with all the software dependencies installed, and shared the git repo folders from your local machine to the image, as such

```
// example ppavp
$ cd ppavp
$ ./build_ppavp.sh

```

You should already have its image 

```
$ docker image ls
REPOSITORY            TAG                     IMAGE ID            CREATED             SIZE
*ppavp                 latest                  cfd717bead9b        4 days ago          2.93GB
nvidia/cuda           latest                  e2728e6295c6        5 days ago          1.96GB
...

```

Now build the dev environment on top of it. By default all IDEs are installed. 

```
$ cd dev-env-tools
$ ./build.sh --image ppavp [--ide=ALL|QT|CLION|NETBEANS|PYCHARM]
``` 

And you will have another image built
```
$ docker image ls
REPOSITORY            TAG                     IMAGE ID            CREATED             SIZE
*dev_ppavp             latest                  ad7085dcf37e        4 days ago          3.13GB
ppavp                 latest                  cfd717bead9b        4 days ago          2.93GB
...

```

Then launch it, and you are inside the dev environment. From there you can navigate the repositories and use your tools. The local repo on your computer is usually shared with the image, so I added a parameter to set it dynamically at start time. 
```
$ ./run_ppavp.sh --local ~/development/ppavp
Starting Development container (PPAVP) ...
Sourcing workspace...
Running:
/bin/sh -c "/bin/bash"
developer@62196fd140d1:~/workspace$ qtcreator &
```

or 

```
developer@62196fd140d1:~/workspace$ pycharm & 
```

or 

```
developer@62196fd140d1:~/workspace$ clion & 
```

or 

```
developer@62196fd140d1:~/workspace$ netbeans &  
```

**Note**: the running script is difficult to automate, so I created one for each of our repos: ppavp, OpenStreetDrone, Autoware. So, temporarily, the scripts work this way

```
./build.sh --image autoware-kinetic --ide QT
```

And
```
./run_autoware.sh kinetic
```







